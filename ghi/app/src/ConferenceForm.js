import React from 'react';

class LocationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            maxPresentations: '',
            maxAttendees: '',
            location: '',
            locations: [],
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartChange = this.handleStartChange.bind(this);
        this.handleEndChange = this.handleEndChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentations = this.handleMaxPresentations.bind(this);
        this.handleMaxAttendees = this.handleMaxAttendees.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }

    handleStartChange(event) {
        const value = event.target.value;
        this.setState({ starts: value })
    }

    handleEndChange(event) {
        const value = event.target.value;
        this.setState({ ends: value })
    }

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({ description: value })
    }

    handleMaxPresentations(event) {
        const value = event.target.value;
        this.setState({ maxPresentations: value })
    }

    handleMaxAttendees(event) {
        const value = event.target.value;
        this.setState({ maxAttendees: value })
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees;
        delete data.maxPresentations;
        delete data.maxAttendees;
        delete data.locations;

        console.log(data);

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                maxPresentations: '',
                maxAttendees: '',
                location: '',
            };
            this.setState(cleared);


        }
    }


    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations })
            console.log(data.locations)


        }
    }


    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new Conference</h1>
                            <form onSubmit={this.handleSubmit} id="create-conference-form">
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name"
                                        className="form-control" />
                                    <label htmlFor="name">Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleStartChange} placeholder="Starts" required type="date" name="starts" id="starts"
                                        className="form-control" />
                                    <label htmlFor="starts">Start </label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleEndChange} placeholder="Ends" required type="date" name="ends" id="ends"
                                        className="form-control" />
                                    <label htmlFor="ends">End </label>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="description" className="form-label">Description</label>
                                    <textarea onChange={this.handleDescriptionChange} className="form-control" name="description" id="description" rows="3"></textarea>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleMaxPresentations} placeholder="Max presentations" required type="number" name="max_presentations"
                                        id="max_presentations" className="form-control" />
                                    <label htmlFor="max_presentations">Maximum Presentations</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleMaxAttendees} placeholder="Max attendees" required type="number" name="max_attendees"
                                        id="max_attendees" className="form-control" />
                                    <label htmlFor="max_attendees">Maximum Attendees</label>
                                </div>
                                <div className="mb-3">
                                    <select onChange={this.handleLocationChange} required id="location" name="location" className="form-select">
                                        <option value="">Choose a location</option>
                                        {this.state.locations.map(location => {
                                            return (
                                                <option key={location.pk} value={location.pk}>
                                                    {location.name}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default LocationForm;